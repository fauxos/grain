export default class Task {
    constructor() {
        this.dj = null;
        this.eventHandlerMap = new Map();
    }
    setDJ(dj) {
        if (this.dj === null) {
            this.dj = dj;
            this.dj.setRecever(this.eventManager);
        }
    }
    eventManager(disk) {
        let handler = this.eventHandlerMap.get(disk.event);
        if (handler) {
            handler(disk);
            return true;
        }
        else {
            return false;
        }
    }
}
