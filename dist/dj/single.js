export default class Single {
    constructor(origin, bracelet, event, data) {
        this.origin = origin;
        this.bracelet = bracelet;
        this.event = event;
        this.data = data;
    }
}
