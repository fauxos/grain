import Single from "./single";
export default class DJ {
    constructor(taskMan, task, bracelet, maxBracelet) {
        this.taskMan = taskMan;
        this.task = task;
        this.bracelet = bracelet;
        this.maxBracelet = maxBracelet || bracelet;
        this.recever = function () { return false; };
    }
    setRecever(callback) {
        this.recever = callback;
    }
    sendMessage(dest, event, data) {
        let disk = new Single(this.task, this.bracelet, event, data);
        return this.taskMan.message(dest, disk);
    }
    getMessage(track) {
        if (track.bracelet < this.maxBracelet) {
            return false;
        }
        return this.recever(track);
    }
}
