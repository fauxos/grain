import BootstrapTask from "./bootstrap/bootstrap_task";
import TaskManager from "./task_manager";
/**
 * Sets up the Grain system.
 * @param bootstrapTask This task starts as system and should **always** be hard coded.
 */
export default class Grain {
    constructor(bootstrapTask) {
        this.taskManager = new TaskManager();
        this.init(bootstrapTask || new BootstrapTask(this));
    }
    /**
     * Jumpstarts the system by running the initial task.
     * @param  bootstrapTask The first task to run, sets up all other starter tasks.
     */
    init(bootstrapTask) {
        this.taskManager.init("core", bootstrapTask, 0);
        bootstrapTask.init();
    }
}
