import DJ from "./dj/dj";
export default class TaskManager {
    constructor() {
        this.tasks = new Map();
        this.taskDJs = new Map();
    }
    check(name) {
        return this.tasks.has(name);
    }
    init(primeName, task, bracelet, ...altNames) {
        let name = primeName;
        if (this.check(name)) {
            name = null;
            for (let altName of altNames) {
                name = altName;
                if (this.check(name)) {
                    name = null;
                }
                else {
                    break;
                }
            }
            if (name === null) {
                throw new Error(`Program  ${primeName} already spawned.`);
            }
        }
        this.tasks.set(name, task);
        let dj = new DJ(this, name, bracelet);
        this.taskDJs.set(name, dj);
        task.setDJ(dj);
    }
    access(name) {
        return this.tasks.get(name) || false;
    }
    message(name, track) {
        let dj = this.taskDJs.get(name);
        if (dj) {
            return dj.getMessage(track);
        }
        else {
            return false;
        }
    }
}
