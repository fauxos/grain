import Disk from "./../dj/disk";
import Task from "./../task/task";
import Grain from "./../grain";

/**
 * A special type of taks that is used during system startup to load other required tasks.
 *
 * Can also be used as a high-privlage task.
 * @param system The system we are loading tasks for.
 */
export default class BootstrapTask extends Task {
  protected system: Grain;

  constructor(system: Grain) {
    super();
    this.system = system;
  }

  /**
   * This is run once the task has been added to the task manager and as such the DJ has been loaded.
   *
   * This is where you load all the other tasks.
   */
  public init(): void {
    this.eventHandlerMap.set("test", (d: Disk) => {console.log(d)})
    console.log("Task running");
    this.dj?.sendMessage("core", "test", "Hello World");
  }
}
