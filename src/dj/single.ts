import Disk from "./disk";
export default class Single implements Disk {
  readonly bracelet: number;
  readonly origin: string;
  readonly event: string;
  readonly data?: any;

  constructor(origin: string, bracelet: number, event: string, data?: any) {
    this.origin = origin;
    this.bracelet = bracelet;
    this.event = event;
    this.data = data;
  }
}
