export default interface Disk {
  bracelet: number,
  origin?: string,
  event: string,
  data?: any
}
