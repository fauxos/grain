import Disk from "./disk";
import Single from "./single";
import TaskManager from "./../task_manager";
export default class DJ {
  private bracelet: number;
  private maxBracelet: number;
  private task: string;
  private taskMan: TaskManager;
  private recever: (d: Disk) => boolean;
  constructor(taskMan: TaskManager, task: string, bracelet: number, maxBracelet?: number) {
    this.taskMan = taskMan;
    this.task = task;
    this.bracelet = bracelet;
    this.maxBracelet = maxBracelet || bracelet;
    this.recever = function() {return false};
  }

  public setRecever(callback: (d: Disk) => boolean){
    this.recever = callback;
  }

  public sendMessage(dest: string, event: string, data?: any): boolean {
    let disk = new Single(this.task, this.bracelet, event, data);
    return this.taskMan.message(dest, disk);
  }

  public getMessage(track: Disk): boolean {
    if(track.bracelet < this.maxBracelet){
      return false;
    }
    return this.recever(track);
  }
}
