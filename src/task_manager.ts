import Task from "./task/task";
import DJ from "./dj/dj";
import Disk from "./dj/disk";

export default class TaskManager {
  private tasks: Map<String, Task>;
  private taskDJs: Map<String, DJ>;
  constructor() {
    this.tasks = new Map();
    this.taskDJs = new Map();
  }

  public check(name: string): boolean {
    return this.tasks.has(name);
  }

  public init(primeName: string, task:Task, bracelet: number, ...altNames:Array<string> ): void {
    let name: string|null = primeName;
    if(this.check(name)){
      name = null;
      for(let altName of altNames){
        name = altName;
        if(this.check(name)) {
          name = null;
        } else {
          break;
        }
      }
      if(name === null){
        throw new Error(`Program  ${primeName} already spawned.`);
      }
    }
    this.tasks.set(name, task);
    let dj = new DJ(this, name, bracelet);
    this.taskDJs.set(name, dj);
    task.setDJ(dj);
  }

  public access(name: string): Task|false{
    return this.tasks.get(name) || false;
  }

  public message(name: string, track:Disk): boolean {
    let dj = this.taskDJs.get(name);
    if(dj){
      return dj.getMessage(track);
    } else {
      return false;
    }
  }
}
