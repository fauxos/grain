import DJ from "./../dj/dj";
import Disk from "./../dj/disk";
export default class Task {
  protected dj: DJ | null;
  protected eventHandlerMap: Map<String, (d: Disk) => void>;
  constructor() {
    this.dj = null;
    this.eventHandlerMap = new Map();
  }

  public setDJ(dj: DJ){
    if(this.dj === null){
      this.dj = dj;
      this.dj.setRecever(this.eventManager);
    }
  }

  protected eventManager(disk: Disk): boolean {
    let handler = this.eventHandlerMap.get(disk.event);
    if(handler){
      handler(disk);
      return true;
    } else {
      return false;
    }
  }
}
